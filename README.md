# CryptOfTheJavaDancer

> Copycat of Crypt Of the NecroDancer using JavaFX for a group project (Made with Neatbeans 8.2).

> By Kévin Deschaud, Alexandre Lithaud, Dorian Morin, Maxime Quinot, Louis Balivet et Robin Petiot.

## **This Project is for educational purposes only !**

## **All rights reserved to Brace Yourself Games**